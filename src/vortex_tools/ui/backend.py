from functools import reduce
from time import time
from typing import List, Iterable, Optional
from enum import Enum

from matplotlib import cm
from matplotlib.axes import Axes
from matplotlib.image import AxesImage
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvas

import numpy as np

from PyQt5 import Qt
from PyQt5.QtWidgets import QWidget
from PyQt5.QtGui import QPainter, QTransform, QImage, QPixmap, QPaintEvent, QFontMetricsF, QPen, QColor
from PyQt5.QtCore import QPointF, QRectF

class BaseImageWidget:
    def __init__(self, *args, **kwargs):
        '''
        cmap `Callable`: `cm.turbo`
            Callable object that maps [0, 1] to a [0, 255] grayscale, grayscale-alpha, RGB, or RGBA value
        range `Iterable[float]`: [0, 1]
            Two-element list of minimum and maximum color map values.
        aspect `float`: 1
            Target ratio of width to height.
        data `numpy.ndarray`: `None`
            Initial Numpy array to display.
        debug `bool`: `False`
            Show sizes and frame rates on the display.
        '''

        self._colormap: np.ndarray[float] = kwargs.pop('cmap', cm.turbo)
        self._range: Iterable[int] = kwargs.pop('range', [0, 1])
        self._aspect: float = kwargs.pop('aspect', 1)

        self.__data: np.ndarray = kwargs.pop('data', None)

        self._debug = kwargs.pop('debug', False)
        self.__frame_times = []

        super().__init__(*args, **kwargs)

    @property
    def data(self):
        return self.__data
    @data.setter
    def data(self, value: np.ndarray):
        self.__data = value
        self.invalidate()

    def invalidate(self):
        raise NotImplementedError

    def _draw_stats(self, painter: QPainter):
        # calculate frame rate
        self.__frame_times.append(time())

        if len(self.__frame_times) < 2:
            fps = 0
        else:
            fps = (len(self.__frame_times) - 1) / (self.__frame_times[-1] - self.__frame_times[0])
        while self.__frame_times[-1] - self.__frame_times[0] > 1:
            del self.__frame_times[0]

        lines = [f'{fps:.1f} fps']
        if self.data is not None:
            lines += ['I: ' + ' x '.join([str(x) for x in self.data.shape])]
        lines += [f'W: {self.width()} x {self.height()}']
        lines += self._make_additional_stats()

        # layout options
        metrics = QFontMetricsF(painter.font())

        offsets = [QPointF(3, (i + 1) * metrics.height()) for i in range(len(lines))]
        bounds =  [metrics.boundingRect(line).translated(offset) for (offset, line) in zip(offsets, lines)]

        max_bounds = reduce(lambda a, b: a.united(b), bounds).adjusted(0, 0, 4, 2)
        max_bounds.setTopLeft(QPointF(0, 0))

        # draw the context
        painter.setPen(QPen(QColor(255, 255, 255)))

        painter.fillRect(max_bounds, QColor(0, 0, 0, 128))
        for (offset, line) in zip(offsets, lines):
            painter.drawText(offset, line)

    def _make_additional_stats(self) -> List:
        return []

class NumpyImageWidget(BaseImageWidget, QWidget):
    class Scaling(Enum):
        Absolute = 0
        Relative = 1
        Percentile = 2

    class Sizing(Enum):
        Fixed = 0
        Fit = 1
        Stretch = 2

    def __init__(self, *args, **kwargs):
        '''
        Efficiently display a Numpy array as a color-mapped image.

        Set the displayed array using the `data` property.
        The array may be edited in-place.
        Call `invalidate()` to request regeneration of the display image when editing the array in place.

        transform `QTransform`: `None`
            Transformation to apply to the displayed image.
            The origin is the center of the display.
            This transformation is applied after the image has been initially scaled.
        scaling `Scaling`: `Relative`
            Interpretation of range.
            `Absolute` indicates fixed values.
            `Relative` indicates values relative to the range of the input data as a ratio in [0, 1].
            `Percentile` indicates percentiles of the input data as a ratio in [0, 1].
        sizing `Iterable[Sizing]`: [`Fit`, `Fit`]
            Control sizing of image within widget in the horizontal and vertical directions, respectively.
            `Fixed` does not scale with the display.
            `Stretch` fills the direction with the image.
            `Fit` uses the largest length of that dimension while respecting the other settings.
        '''

        self._transform = kwargs.pop('transform', None)
        if not self._transform:
            self._transform = QTransform()

        self._range_mode: NumpyImageWidget.Scaling = kwargs.pop('scaling', NumpyImageWidget.Scaling.Relative)
        self._size_mode: Iterable[NumpyImageWidget.Sizing] = kwargs.pop('sizing', [NumpyImageWidget.Sizing.Fit, NumpyImageWidget.Sizing.Fit])
        self._crosshairs = kwargs.pop('crosshairs', False)

        self.__image: QImage = None

        super().__init__(*args, **kwargs)

    def paintEvent(self, e: QPaintEvent) -> None:
        painter = QPainter(self)

        # background
        super().paintEvent(e)

        # foreground
        if self.image:
            painter.save()
            painter.setTransform(self._make_draw_transform())
            painter.drawPixmap(QPointF(-self.image.width() / 2.0, -self.image.height() / 2.0), self.image)
            painter.restore()

        if self._crosshairs:
            painter.save()
            painter.setCompositionMode(QPainter.CompositionMode.CompositionMode_Exclusion)
            painter.setPen(QPen(QColor(255, 255, 255)))
            painter.drawLine(0, self.height() / 2, self.width(), self.height() / 2)
            painter.drawLine(self.width() / 2, 0, self.width() / 2, self.height())
            painter.restore()

        if self._debug:
            self._draw_stats(painter)

    def invalidate(self) -> None:
        '''
        Clear the cached image.
        This is required for changes to the underlying data buffer to display.
        '''
        self.__image = None

    @property
    def image(self) -> QPixmap:
        if not self.__image:
            self._make_and_cache_image()

        # get from cache
        return self.__image

    def _make_and_cache_image(self) -> None:
        if self.data is None:
            return

        shape = self.data.shape

        xform = self._make_draw_transform(shape)
        img_rect = QRectF(-shape[1] / 2.0, -shape[0] / 2.0, shape[1], shape[0])
        wnd_rect = xform.mapRect(img_rect)

        # clip to displayed region
        tl = wnd_rect.topLeft()
        br = wnd_rect.bottomRight()

        tl.setX(np.clip(tl.x(), 0, self.width()))
        tl.setY(np.clip(tl.y(), 0, self.height()))
        br.setX(np.clip(br.x(), 0, self.width()))
        br.setY(np.clip(br.y(), 0, self.height()))

        win_roi_rect = QRectF(tl, br)

        img_roi_rect = xform.inverted()[0].mapRect(win_roi_rect)
        img_roi_rect.translate(shape[1] / 2.0, shape[0] / 2.0)

        start = np.asanyarray([
            np.clip(round(img_roi_rect.topLeft().y()), 0, shape[0]),
            np.clip(round(img_roi_rect.topLeft().x()), 0, shape[1])
        ])
        end = np.asanyarray([
            np.clip(round(img_roi_rect.bottomRight().y()), 0, shape[0]),
            np.clip(round(img_roi_rect.bottomRight().x()), 0, shape[1])
        ])

        # check if nothing to draw
        if (end - start).min() < 0:
            return

        # extract region of interest
        step = (end - start + 1) // np.asanyarray([ win_roi_rect.height(), win_roi_rect.width() ]).round().astype(int)
        step = np.where(step < 1, 1, step)
        data = self.data[start[0]:end[0]:step[0], start[1]:end[1]:step[1], ...]

        # determine scale bounds on full data
        if self._range_mode == NumpyImageWidget.Scaling.Absolute:
            (vmin, vmax) = self._range
        elif self._range_mode == NumpyImageWidget.Scaling.Relative:
            vmin = self.data.min()
            vmax = self.data.max()
        elif self._range_mode == NumpyImageWidget.Scaling.Percentile:
            (vmin, vmax) = np.percentile(self.data, self._range)
        else:
            raise ValueError(f'unknown data range mode: {self._range_mode}')

        # apply scale and colomap
        data = (data.astype(np.float32) - vmin) / (vmax - vmin)
        data = self._colormap(data, bytes=True)

        # _range image format
        data = np.squeeze(data)
        if data.ndim in [1, 2]:
            color = False
            alpha = False
        elif data.ndim == 3:
            color = data.shape[2] >= 3
            alpha = data.shape[2] in [2, 4]
        else:
            raise ValueError(f'invalid image dimensions: {data.ndim}')

        # match Qt optimized image format
        data = np.atleast_3d(data)
        if color:
            if alpha:
                # native format already
                pass
            else:
                data = np.concatenate((data, np.full(data.shape[:2], 255)))
        else:
            if alpha:
                data = np.take(data, [0, 0, 0, 1], axis=2)
            else:
                data = np.concatenate((data, data, data, np.full(data.shape[:2], 255)))

        # convert to QImage
        # data = cbook._unmultiplied_rgba8888_to_premultiplied_argb32(memoryview(data))
        # self.__image = QPixmap.fromImage(QImage(data, data.shape[1], data.shape[0], QImage.Format_ARGB32_Premultiplied))
        self.__image = QPixmap.fromImage(QImage(data, data.shape[1], data.shape[0], QImage.Format_RGBA8888))

    def _make_draw_transform(self, shape: Optional[Iterable[int]]=None) -> QTransform:
        if shape is None:
            image_height = self.image.height()
            image_width = self.image.width()
        else:
            (image_height, image_width) = shape

        xform = QTransform()
        # start by drawing a 1x1 image in the widget center
        xform.translate(self.width() / 2, self.height() / 2)
        xform.scale(1 / image_width, 1 / image_height)

        (xs, ys) = self._size_mode

        if xs == NumpyImageWidget.Sizing.Fixed:
            width = image_width
        elif xs == NumpyImageWidget.Sizing.Stretch:
            width = self.width()
        elif xs == NumpyImageWidget.Sizing.Fit:
            width = None
        else:
            raise ValueError(f'unknown size mode: {xs}')

        if ys == NumpyImageWidget.Sizing.Fixed:
            height = image_height
        elif ys == NumpyImageWidget.Sizing.Stretch:
            height = self.height()
        elif ys == NumpyImageWidget.Sizing.Fit:
            height = None
        else:
            raise ValueError(f'unknown size mode: {ys}')

        if height and width:
            pass
        elif height:
            width = self._aspect * height
        elif width:
            height = width / self._aspect
        else:
            # fit image into widget
            s = min([self.width() / (image_height * self._aspect), self.height() / image_height])
            height = s * image_height
            width = self._aspect * height

        # scale up to actual display size
        xform.scale(width, height)

        # return composed with user transform
        return xform * self._transform

    def _make_additional_stats(self) -> List:
        if self.__image:
            return [f'D: {self.__image.height()} x {self.__image.width()}']
        else:
            return []

class MatplotlibImageWidget(BaseImageWidget, FigureCanvas):
    def __init__(self, *args, **kwargs):
        '''
        Display a Numpy array using Matplotlib.

        Set the displayed array using the `data` property.
        The array may be edited in-place as long as `invalidate()` is called afterwards.

        figsize `Tuple[float]`: (6, 4)
            Size of figure for standalone windows.
        '''

        figsize = kwargs.pop('figsize', (6.5, 5))

        self._axes: Axes = None
        self.__axes_image: AxesImage = None
        self.__invalidated: bool = True

        super().__init__(Figure(figsize=figsize), *args, **kwargs)

    def paintEvent(self, e: QPaintEvent):
        # NOTE: redraw only when invalidated to avoid continuous drawing
        if self.axes_image and self.__invalidated:
            self.draw()
            self.__invalidated = False

        super().paintEvent(e)

        if self._debug:
            painter = QPainter(self)
            self._draw_stats(painter)

    def invalidate(self):
        '''
        Request a redraw to update the image.
        '''
        if self.axes_image:
            self.axes_image.set_array(self.data)
            # NOTE: do not draw here because this is likely called from a Vortex thread
            self.__invalidated = True
            self.update()

    @property
    def axes_image(self):
        if not self.__axes_image or (self.data is not None and self.data.shape != self.__axes_image.get_array().shape):
            self._make_and_cache_axes()

        # get from cache
        return self.__axes_image

    @property
    def axes(self):
        return self._axes

    def _make_and_cache_axes(self):
        if self.data is None:
            return

        if self.__axes_image is not None:
            # destroy the old axes
            self.figure.clear()

        # generate the plot
        self._axes = self.figure.subplots()
        self.__axes_image = self._axes.imshow(
            self.data,
            interpolation='nearest',
            vmin=self._range[0], vmax=self._range[1],
            aspect=self._aspect,
            cmap=self._colormap
        )

        self.figure.tight_layout()

if __name__ == '__main__':
    from PyQt5.QtCore import Qt, QCoreApplication, QTimer
    from PyQt5.QtGui import QGuiApplication
    from PyQt5.QtWidgets import QApplication

    QCoreApplication.setAttribute(Qt.AA_UseHighDpiPixmaps)
    QGuiApplication.setAttribute(Qt.AA_EnableHighDpiScaling)

    import sys
    app = QApplication(sys.argv)
    app.setQuitOnLastWindowClosed(True)

    # cause KeyboardInterrupt to exit the Qt application
    import signal
    signal.signal(signal.SIGINT, lambda sig, frame: app.exit())

    # regularly re-enter Python so the signal handler runs
    def keepalive(msec):
        QTimer.singleShot(msec, lambda: keepalive(msec))
    keepalive(10)

    (x, y) = np.meshgrid(*[np.linspace(0, 1, n) for n in [10, 20]])
    data = x + y

    names = ['MatplotlibImageWidget', 'NumpyImageWidget']
    widgets = [globals()[n](data=data, debug=True) for n in names]

    for (n, w) in zip(names, widgets):
        w.setWindowTitle(n)
        w.setStyleSheet('background: magenta;')
        w.show()

    app.exec()
